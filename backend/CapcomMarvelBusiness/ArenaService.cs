﻿using DAL.Repositories;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapcomMarvelBusiness
{
    public class ArenaService : IArenaService
    {
        private readonly IArenaRepository _ArenaRepository;

        public ArenaService(IArenaRepository repo)
        {
            _ArenaRepository = repo;
        }
        public ArenaDTO GetArenaById(int ArenaId)
        {
            return _ArenaRepository.GetArenaById(ArenaId);
        }

        public List<ArenaDTO> GetArenas()
        {
            return _ArenaRepository.GetArenas();
        }

        public void AddArena(ArenaDTO Arena)
        {
            _ArenaRepository.AddArena(Arena);
        }

        public void RemoveArena(int id)
        {
            _ArenaRepository.RemoveArena(id);
        }

        public void UpdateArena(int id, ArenaDTO Arena)
        {
            _ArenaRepository.UpdateArena(id, Arena);
        }
    }
}
