﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapcomMarvelBusiness
{
    public interface IArenaService
    {
        ArenaDTO GetArenaById(int ArenaId);
        List<ArenaDTO> GetArenas();
        void AddArena(ArenaDTO Arena);
        void RemoveArena(int id);
        void UpdateArena(int id, ArenaDTO Arena);
    }
}
