﻿using DTO;

namespace CapcomMarvelBusiness
{
    public interface ICharacterService
    {
        CharacterDTO GetCharacterByName(String characterName);
        List<CharacterDTO> GetCharacters();
        void AddCharacter(CharacterDTO character);
        void RemoveCharacter(string name);
        void UpdateCharacter(string name, CharacterDTO character);
    }
}