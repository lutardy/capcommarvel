﻿using DAL.Models;
using DAL.Repositories;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapcomMarvelBusiness
{
    public class CharacterService : ICharacterService
    {
        private readonly ICharacterRepository _characterRepository;

        public CharacterService(ICharacterRepository repo)
        {
            _characterRepository = repo;
        }
        public CharacterDTO GetCharacterByName(string characterName)
        {
            return _characterRepository.GetCharacterByName(characterName);
        }

        public List<CharacterDTO> GetCharacters()
        {
            return _characterRepository.GetCharacters();
        }

        public void AddCharacter(CharacterDTO character)
        {
            _characterRepository.AddCharacter(character);
        }

        public void RemoveCharacter(string name)
        {
            _characterRepository.RemoveCharacter(name);
        }

        public void UpdateCharacter(string name, CharacterDTO character)
        {
            _characterRepository.UpdateCharacter(name, character);
        }
    }
}
