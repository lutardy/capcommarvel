﻿using DAL.Models;
using DTO;
using GameCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore
{
     public static class Extensions
    {
        #region Session
        public static SessionDTO ToDto(this Session entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            Dictionary<uint, Player> players = entity.GetPlayers();

            var playersDTO = new Dictionary<uint, PlayerDTO>();
            foreach (var item in players)
            {
                playersDTO.Add(item.Key, ToDto(item.Value));
            }
            Arena arena = entity.GetArena();
            return new SessionDTO
            {
                Id = entity.Id,
                Players = playersDTO,
                Current = entity.GetCur(),
                Winner = entity.GetWinner(),
                Arena = arena.ToDto(),
            };
        }
        #endregion

        #region Arena
        public static ArenaDTO ToDto(this Arena entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return new ArenaDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Sprite = entity.Sprite,
                Icon = entity.Icon,
                Description = entity.Description
            };
        }
        #endregion

        #region Player
        public static PlayerDTO ToDto(this Player entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            return new PlayerDTO
            {
                id = entity.id,
                name = entity.name,
                charac = entity.charac.ToDto(),
                currentCharacter = entity.currentCharacter,
            };
        }
        #endregion

        #region Character
        public static List<CharacterDTO> ToDto(this List<Character> list)
        {
            if (list == null || !list.Any())
                throw new ArgumentNullException(nameof(list));
            List<CharacterDTO> result = new List<CharacterDTO>();
            list.ForEach(x => result.Add(x.ToDto()));
            return result;
        }
        public static CharacterDTO ToDto(this Character entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return new CharacterDTO
            {
                Name = entity.Name,
                Attack = entity.StartAttack,
                Lp = entity.StartLp,
                Sprite = entity.Sprite,
                Icon = entity.Icon,
                Description = entity.Description,
                hasHealed = entity.hasHealed,
                CurrentAttack = entity.CurrentAttack,
                CurrentLp = entity.CurrentLp,
            };
        }
        #endregion
    }
}
