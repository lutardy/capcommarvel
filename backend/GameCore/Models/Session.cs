﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Models
{
    public class Session
    {
        static private uint nid = 0;
        static private List<Session> sessions = new List<Session>();
        public uint Id { get; }
        private Dictionary<uint, Player> players;
        private uint cur;
        private int winner;
        private Arena arena;

        public Session(uint id, Player[] p, uint c, Arena a)
        {
            Id = id;
            players = new Dictionary<uint, Player>();
            for (uint i = 0; i < p.Length; i++)
                players.Add(p[i].id, p[i]);
            cur = c;
            winner = -1;
            arena = a;
            sessions.Add(this);
        }
        public Session(Player[] p, Arena a) : this(nid++, p, p[0].id, a) { }

        public Dictionary<uint, Player> GetPlayers()
        {
            return players;
        }

        public uint GetCur()
        {
            return cur;
        }

        public void NextCur()
        { 
            List<uint> keys = players.Keys.ToList();
            Player win = players[keys[new Random().Next(keys.Count)]];
            cur = win.id;
        }

        public void DoHeal()
        {
            try
            {
                players[cur].DoHeal();
                NextCur();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            
        }

        public void DoDamages()
        {
            Player attacker = players[cur];
            foreach (Player p in players.Values)
            {
                Character c = p.charac[p.currentCharacter];
                if (p.id != cur)
                {
                    c.SetLP(c.CurrentLp - attacker.charac[attacker.currentCharacter].GetAttack());
                }
                if (c.CurrentLp <= 0)
                {
                    if (p.ChangeCharac())
                    {
                        winner = (int)cur;
                    }
                }
            }
            NextCur();
        }
        public int GetWinner()
        {
            return winner;
        }

        public Arena GetArena() { return arena; }

        public static Session GetSessionById(uint id)
        {
            int i = 0;
            Session? session = null;
            while(session == null && i < sessions.Count)
            {
                if (sessions[i].Id == id)
                    session = sessions[i];
                i++;
            }
            if(session == null)
            {
                throw new NullReferenceException("Session inexistante");
            }
            return session;
        }

        public static void DeleteSessionById(uint id)
        {
            try
            {
                Session session = GetSessionById(id);
                sessions.Remove(session);
            }
            catch (NullReferenceException)
            {
                throw;
            }
            
        }

        public static void ResetSession(uint id)
        {
            try
            {
                Session session = GetSessionById(id);
                foreach(Player p in session.players.Values)
                {
                    foreach(Character c in p.charac)
                        c.Reset();
                    p.currentCharacter = 0;
                }
                session.winner = -1;
                session.NextCur();
            }
            catch (NullReferenceException)
            {
                throw;
            }
        }

        public static void Swap(uint IdSession, int NewCharac)
        {
            try
            {
                Session session = GetSessionById(IdSession);
                session.players[session.cur].Swap(NewCharac);
                session.NextCur();
            } catch (Exception e)
            {
                if (e.GetType() == typeof(NullReferenceException) || e.GetType() == typeof(InvalidOperationException) || e.GetType() == typeof(KeyNotFoundException))
                    throw;
            }
        }
    }
}
