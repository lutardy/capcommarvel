﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Models
{
    public class Character
    {
        public string Name { get; set; } = null!;
        public int StartLp { get; set; }
        public int StartAttack { get; set; }

        public int CurrentLp { get; set; }
        public int CurrentAttack { get; set; } 
        public string? Sprite { get; set; }
        public string? Icon { get; set; }
        public string? Description { get; set; }
        public bool hasHealed { get; set; }

        public Character(CharacterDAL dal, int arenaId)
        {
            Name = dal.Name;
            StartLp = dal.Lp;
            StartAttack = dal.Attack;
            Sprite = dal.Sprite;
            Icon = dal.Icon;
            Description = dal.Description;

            if (arenaId == dal.BonusArena)
            {
                StartLp += dal.BonusLp;
                StartAttack += dal.BonusAttack;
            }

            CurrentAttack = StartAttack;
            CurrentLp = StartLp;
        }

        public void SetLP(int newLP)
        {
            CurrentLp = newLP;
        }

        public void Heal()
        {
            if (hasHealed)
                throw new InvalidOperationException();
            CurrentLp = StartLp;
            hasHealed = true;
        }

        public int GetAttack()
        {
            return CurrentAttack;
        }

        public void Reset()
        {
            hasHealed = false;
            CurrentLp = StartLp;
            CurrentAttack = StartAttack;
        }
    }
}
