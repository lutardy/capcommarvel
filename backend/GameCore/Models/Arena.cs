﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore.Models
{
    public class Arena
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Sprite { get; set; }
        public string? Icon { get; set; }
        public string? Description { get; set; }


        public Arena(ArenaDAL arenaDAL)
        {
            Id = arenaDAL.Id;
            Name = arenaDAL.Name;
            Sprite = arenaDAL.Sprite;    
            Icon = arenaDAL.Icon;
            Description = arenaDAL.Description;
        }
    }
}
