﻿
using DAL.Models;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace GameCore.Models
{
    public class Player
    {
        static private uint nid = 0;
        public uint id { get; }
        public string name { get; }
        public List<Character> charac { get; }

        public int currentCharacter;

        public Player(uint newId, string pname, List<Character> c)
        {
            id = newId;
            name = pname;
            charac = c;
            currentCharacter = 0;
        }
        public Player(string pname, List<Character> c): this(nid++, pname, c) { }

       public void DoHeal()
        {
            try
            {
                charac[currentCharacter].Heal();
            } catch (InvalidOperationException)
            {
                throw;
            }
        }

        public bool ChangeCharac()
        {
            int i = 0;
            while(i < charac.Count && charac[i].CurrentLp <= 0)
            {
                i++;
            }
            currentCharacter = i;
            return i == charac.Count;
        }

        public void Swap(int NewCharac)
        {
            if(charac[NewCharac].CurrentLp <= 0)
            {
                throw new InvalidOperationException();
            }
            currentCharacter = NewCharac;
        }
    }
}

