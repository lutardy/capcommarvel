﻿namespace DTO
{
    public class CharacterDTO
    {
        public string Name { get; set; } = null!;
        public int Lp { get; set; }
        public int Attack { get; set; }
        public int CurrentLp { get; set; }
        public int CurrentAttack { get; set; }
        public string? Sprite { get; set; }
        public string? Icon { get; set; }
        public string? Description { get; set; }
        public int BonusAttack { get; set; }
        public int BonusLp { get; set; }
        public int? BonusArena { get; set; }
        public bool hasHealed { get; set; }
    }
}