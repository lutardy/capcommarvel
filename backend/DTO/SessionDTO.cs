﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SessionDTO
    {
        public uint Id { get; set; }
        public Dictionary<uint, PlayerDTO>? Players { get; set; }
        public uint Current { get; set; }
        public int Winner { get; set; }
        public ArenaDTO Arena { get; set; }
    }
}
