﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PlayerDTO
    {
        public uint id { get; set; }
        public string name { get; set; }
        public List<CharacterDTO> charac { get; set; }
        public int currentCharacter { get; set; }
    }
}
