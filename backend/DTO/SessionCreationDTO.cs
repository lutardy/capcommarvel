﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public partial class SessionCreationDTO
    {
        public List<string> names { get; set; }
        public List<string> characters1 { get; set; }
        public List<string> characters2 { get; set; }
        public int arena { get; set; }
    }
}
