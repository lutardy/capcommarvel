﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DAL.Models
{
    public partial class Context : DbContext
    {
        public Context()
        {
        }

        public Context(DbContextOptions<Context> options)
            : base(options)
        {
        }

        public virtual DbSet<ArenaDAL> Arenas { get; set; } = null!;
        public virtual DbSet<CharacterDAL> Characters { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=tcp:cdiesewebserveur.database.windows.net,1433;Initial Catalog=Capcom vs Marvel;Persist Security Info=False;User ID=administrateur;Password=bLClLsLlT378;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArenaDAL>(entity =>
            {
                entity.ToTable("Arena");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Icon)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Sprite)
                    .HasMaxLength(120)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CharacterDAL>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PK__Characte__737584F7696FFF8E");

                entity.ToTable("Character");

                entity.Property(e => e.Name)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.BonusArena).HasColumnName("Bonus Arena");

                entity.Property(e => e.BonusAttack).HasColumnName("Bonus Attack");

                entity.Property(e => e.BonusLp).HasColumnName("Bonus Lp");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Icon)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Sprite)
                    .HasMaxLength(120)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
