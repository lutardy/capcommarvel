﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public partial class ArenaDAL
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Sprite { get; set; }
        public string? Icon { get; set; }
        public string? Description { get; set; }
    }
}
