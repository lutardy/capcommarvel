﻿using DAL.Models;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class Extensions
    {

        
        #region Character
        public static List<CharacterDTO> ToDto(this List<CharacterDAL> list)
        {
            if (list == null || !list.Any())
                throw new ArgumentNullException(nameof(list));
            List<CharacterDTO> result = new List<CharacterDTO>();
            list.ForEach(x => result.Add(x.ToDto()));
            return result;
        }
        public static CharacterDTO ToDto(this CharacterDAL entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return new CharacterDTO
            {
                Name = entity.Name,
                Attack = entity.Attack,
                Lp = entity.Lp,
                Sprite = entity.Sprite,
                Icon = entity.Icon,
                Description = entity.Description,
                BonusLp = entity.BonusLp,
                BonusAttack = entity.BonusAttack,
                BonusArena = entity.BonusArena
            };
        }

        public static CharacterDAL ToEntity(this CharacterDTO dto)
        {
            if (dto == null)
                throw new ArgumentNullException(nameof(dto));

            return new CharacterDAL
            {
                Name = dto.Name,
                Attack = dto.Attack,
                Lp = dto.Lp,
                Sprite = dto.Sprite,
                Icon = dto.Icon,
                Description = dto.Description,
                BonusArena = dto.BonusArena,
                BonusAttack= dto.BonusAttack,
                BonusLp= dto.BonusLp,
            };
        }
        #endregion

        #region Arena
        public static List<ArenaDTO> ToDto(this List<ArenaDAL> list)
        {
            if (list == null && !list.Any())
                throw new ArgumentNullException(nameof(list));
            List<ArenaDTO> result = new List<ArenaDTO>();
            list.ForEach(x => result.Add(x.ToDto()));
            return result;
        }
        public static ArenaDTO ToDto(this ArenaDAL entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return new ArenaDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Sprite = entity.Sprite,
                Icon = entity.Icon,
                Description = entity.Description
            };
        }

        public static ArenaDAL ToEntity(this ArenaDTO dto)
        {
            if (dto == null)
                throw new ArgumentNullException(nameof(dto));

            return new ArenaDAL
            {
                Id = dto.Id,
                Name = dto.Name,
                Sprite = dto.Sprite,
                Icon = dto.Icon,
                Description = dto.Description
            };
        }

        #endregion

    }
}
