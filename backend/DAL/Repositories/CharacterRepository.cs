﻿using DAL.Models;
using DTO;

namespace DAL.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        //DI 
        private readonly Context _context;
        public CharacterRepository(Context context)
        {
            _context = context;
        }

        public CharacterDTO GetCharacterByName(string name)
        {
            CharacterDTO character = null;
            try
            {
                character = _context.Characters.Single(x => x.Name == name).ToDto();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            return character;
        }

        public List<CharacterDTO> GetCharacters()
        {
            return _context.Characters.ToList().ToDto();
        }

        public void AddCharacter(CharacterDTO character)
        {
            _context.Characters.Add(character.ToEntity());
            _context.SaveChanges();
        }

        public void RemoveCharacter(string name)
        {
            try
            {
                _context.Remove(_context.Characters.Single(x => x.Name == name));
                _context.SaveChanges();
            }
            catch(InvalidOperationException)
            {
                throw;
            }
        }

        public void UpdateCharacter(string name, CharacterDTO dto)
        {
            CharacterDAL character;
            try
            {
                character = _context.Characters.Single(x => x.Name == name);
                character.Lp = dto.Lp;
                character.Attack = dto.Attack;
                character.Sprite = dto.Sprite;
                character.Icon = dto.Icon;
                character.Description = dto.Description;
                character.BonusArena = dto.BonusArena;
                character.BonusAttack = dto.BonusAttack;
                character.BonusLp = dto.BonusLp;
            }
            catch (Exception e) 
            {
                Console.WriteLine(e.Message);
                throw;
            }
            
            _context.SaveChanges();
        }
    }
}
