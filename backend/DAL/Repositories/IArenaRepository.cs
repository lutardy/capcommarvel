﻿using DTO;

namespace DAL.Repositories
{
    public interface IArenaRepository
    {
        ArenaDTO GetArenaById(int id);
        List<ArenaDTO> GetArenas();
        void AddArena(ArenaDTO Arena);
        void RemoveArena(int id);
        void UpdateArena(int id, ArenaDTO Arena);
    }
}