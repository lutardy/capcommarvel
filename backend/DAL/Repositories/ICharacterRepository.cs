﻿using DTO;

namespace DAL.Repositories
{
    public interface ICharacterRepository
    {
        CharacterDTO GetCharacterByName(string name);
        List<CharacterDTO> GetCharacters();
        void AddCharacter(CharacterDTO character);
        void RemoveCharacter(string name);
        void UpdateCharacter(string name, CharacterDTO character);
    }
}