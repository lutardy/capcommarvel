﻿using DAL.Models;
using DTO;

namespace DAL.Repositories
{
    public class ArenaRepository : IArenaRepository
    {
        //DI 
        private readonly Context _context;
        public ArenaRepository(Context context)
        {
            _context = context;
        }

        public ArenaDTO GetArenaById(int id)
        {
            ArenaDTO Arena = null;
            try
            {
                Arena = _context.Arenas.Single(x => x.Id == id).ToDto();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            return Arena;
        }

        public List<ArenaDTO> GetArenas()
        {
            return _context.Arenas.ToList().ToDto();
        }

        public void AddArena(ArenaDTO Arena)
        {
            _context.Arenas.Add(Arena.ToEntity());
            _context.SaveChanges();
        }

        public void RemoveArena(int id)
        {
            try
            {
                _context.Remove(_context.Arenas.Single(x => x.Id == id));
                _context.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        public void UpdateArena(int id, ArenaDTO dto)
        {
            ArenaDAL Arena = null;
            try
            {
                Arena = _context.Arenas.Single(x => x.Id == id);

                Arena.Name = dto.Name;
                Arena.Sprite = dto.Sprite;
                Arena.Icon = dto.Icon;
                Arena.Description = dto.Description;

                Arena = dto.ToEntity();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw;
            }

            _context.SaveChanges();
        }
    }
}
