﻿using CapcomMarvelBusiness;
using DAL;
using DAL.Models;
using DTO;
using GameCore.Models;
using GameCore;
using Microsoft.AspNetCore.Mvc;

namespace WebApiCapcomMarvel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {

        private readonly ICharacterService _characterService;
        private readonly IArenaService _arenaService;


        public SessionController(ICharacterService service1, IArenaService service2)
        {
            _characterService = service1;
            _arenaService = service2;
        }

        // GET : /api/Session/nextplayer/{idSession}
        /*
        [HttpGet("nextplayer/{idSession}")]
        public int GetNextPlayer(uint idSession)
        {
            int result = -1;
            Session session;
            try
            {
                session = Session.GetSessionById(idSession);
                result = (int) session.NextCur();
            }
            catch(NullReferenceException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }

            return result;
        }
        */
        // PUT : /api/Session/attack/{idSession}
        [HttpPut("attack/{idSession}")]
        public void Attack(uint idSession)
        {
            Session session;
            try
            {
                session = Session.GetSessionById(idSession);
                session.DoDamages();
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(NullReferenceException))
                    Response.StatusCode = StatusCodes.Status404NotFound;
                if (e.GetType() == typeof(ArgumentOutOfRangeException))
                    Response.StatusCode = StatusCodes.Status403Forbidden;
            }
        }

        // PUT : /api/Session/heal/{idSession}
        [HttpPut("heal/{idSession}")]
        public void Heal(uint idSession)
        {
            Session session;
            try
            {
                session = Session.GetSessionById(idSession);
                session.DoHeal();
            }
            catch (NullReferenceException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status403Forbidden;
            }
        }

        // GET : /api/Session/{idSession}
        [HttpGet("{idSession}")]
        public SessionDTO GetSessionStatus(uint idSession)
        {
            Session session;
            SessionDTO sessionDTO = new();
            try
            {
                session = Session.GetSessionById(idSession);
                sessionDTO = session.ToDto();
            }
            catch (NullReferenceException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            catch (ArgumentNullException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            return sessionDTO;
        }

        // POST : /api/Session
        [HttpPost()]
        public void CreateSession([FromBody] SessionCreationDTO dto)
        {
            int arenaId = dto.arena;
            string name1 = dto.names[0];
            string name2 = dto.names[1];
            List<string> char1 = dto.characters1;
            List<string> char2 = dto.characters2;
            try
            {
                List<Character> charList = new();
                foreach (string character in char1)
                    charList.Add(new Character(_characterService.GetCharacterByName(character).ToEntity(), arenaId));
                Player p1 = new(name1, charList);

                charList = new List<Character>();
                foreach (string character in char2)
                    charList.Add(new Character(_characterService.GetCharacterByName(character).ToEntity(), arenaId));
                Player p2 = new(name2, charList);

                Arena arena = new(_arenaService.GetArenaById(arenaId).ToEntity());
                Player[] players = { p1, p2 };
                Session session = new(players, arena);
                Response.StatusCode = StatusCodes.Status201Created;
                session.NextCur();
                Response.Headers.Add("Access-Control-Expose-Headers", "id");
                Response.Headers.Add("id", session.Id.ToString());
            }
            catch (Exception e)
            {
                if(e.GetType() == typeof(NullReferenceException) || e.GetType() == typeof(InvalidOperationException) || e.GetType() == typeof(ArgumentNullException))
                    Response.StatusCode = StatusCodes.Status400BadRequest;
            }
        }

        // DELETE : /api/Session/{idSession}
        [HttpDelete("{idSession}")]
        public void DeleteSession(uint idSession)
        {
            try
            {
                Session.DeleteSessionById(idSession);
            }
            catch(NullReferenceException)
            {
                Response.StatusCode= StatusCodes.Status404NotFound;
            }
        }

        // PUT : /api/Session/ResetSession/{idSession}
        [HttpPut("resetSession/{idSession}")]
        public void ResetSession(uint idSession)
        {
            try
            {
                Session.ResetSession(idSession);
            }
            catch (NullReferenceException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
        }

        // PUT : /api/Session/ResetSession/{idSession}
        [HttpPut("Swap/{idSession}")]
        public void Swap(uint idSession, int NewCharac)
        {
            try
            {
                Session.Swap(idSession, NewCharac);
            }catch (Exception e)
            {
                if (e.GetType() == typeof(NullReferenceException) || e.GetType() == typeof(KeyNotFoundException))
                    Response.StatusCode = StatusCodes.Status404NotFound;
                if (e.GetType() == typeof(InvalidOperationException))
                    Response.StatusCode = StatusCodes.Status403Forbidden;
            }
        }
    }
}
