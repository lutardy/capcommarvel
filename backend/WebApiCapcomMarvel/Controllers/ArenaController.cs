﻿
using Microsoft.AspNetCore.Mvc;
using DTO;
using CapcomMarvelBusiness;

namespace WebApiCapcomMarvel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArenaController : ControllerBase
    {
        private readonly IArenaService _service;

        public ArenaController(IArenaService service)
        {
            _service = service;
        }

        // GET: api/Arena
        [HttpGet()]
        public List<ArenaDTO> GetArenas()
        {
            List<ArenaDTO> result = _service.GetArenas();
            if (result.Count == 0)
                Response.StatusCode = StatusCodes.Status204NoContent;
            return result;
        }

        // GET: api/Arena/{id}
        [HttpGet("{id}")]
        public ArenaDTO GetArena(int id)
        {
            ArenaDTO arena = null;
            try
            {
                arena = _service.GetArenaById(id);
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            return arena;
        }
        // PUT: api/Arena/{id}
        [HttpPut("{id}")]
        public void PutArena(int id, ArenaDTO Arena)
        {
            try
            {
                _service.UpdateArena(id, Arena);
            }
            catch(InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
        }

        // POST: api/Arena
        [HttpPost()]
        public void PostArena(ArenaDTO Arena)
        {
            try
            {
                _service.AddArena(Arena);
                Response.StatusCode = StatusCodes.Status201Created;
            }
            catch (ArgumentNullException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException)
            {
                Response.StatusCode = StatusCodes.Status403Forbidden;
            }
        }

        // DELETE: api/Arena/{id}
        [HttpDelete("{id}")]
        public void DeleteArena(int id)
        {
            try
            {
                _service.RemoveArena(id);
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
        }
    }
}
