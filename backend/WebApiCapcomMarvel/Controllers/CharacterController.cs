﻿
using Microsoft.AspNetCore.Mvc;
using DTO;
using CapcomMarvelBusiness;

namespace WebApiCapcomMarvel.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterService _service;

        public CharacterController(ICharacterService service)
        {
            _service = service;
        }

        // GET: api/Character
        [HttpGet()]
        public List<CharacterDTO> GetCharacters()
        {
            List<CharacterDTO> list = _service.GetCharacters();
            if (list.Count == 0)
                Response.StatusCode = StatusCodes.Status204NoContent;
            return list;
        }

        // GET: api/Character/{name}
        [HttpGet("{name}")]
        public CharacterDTO GetCharacter(string name)
        {
            CharacterDTO character = null;
            try
            {
                character = _service.GetCharacterByName(name);
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;   
            }
            return character;
        }
        // PUT: api/Character/{name}
        [HttpPut("{name}")]
        public void PutCharacter(string name, CharacterDTO character)
        {
            try
            {
                _service.UpdateCharacter(name, character);
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            
        }

        // POST: api/Character
        [HttpPost()]
        public void PostCharacter(CharacterDTO character)
        {
            try
            {
                _service.AddCharacter(character);
                Response.StatusCode = StatusCodes.Status201Created;
            }
            catch (ArgumentNullException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
            catch (Microsoft.EntityFrameworkCore.DbUpdateException)
            {
                Response.StatusCode = StatusCodes.Status403Forbidden;
            }
        }

        // DELETE: api/Character/{name}
        [HttpDelete("{name}")]
        public void DeleteCharacter(string name)
        {
            try
            {
                _service.RemoveCharacter(name);
            }
            catch (InvalidOperationException)
            {
                Response.StatusCode = StatusCodes.Status404NotFound;
            }
        }
    }
}
