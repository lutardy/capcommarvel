import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  constructor(private router: Router, private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('Capcom VS Marvel');
  }

  onNew(): void {
    this.router.navigateByUrl('/new');
  }

  onAdmin(): void {
    this.router.navigateByUrl('/admin');
  }
}
