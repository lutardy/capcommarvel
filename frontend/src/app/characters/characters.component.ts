import { Component, OnInit } from '@angular/core';
import { Character } from '../classes/character';
import { CharacterService } from '../services/character.service';
import { Router } from '@angular/router';
import { ErrorComponent } from '../error/error.component';

type CharacterChoice = {
  char: Character;
  selected: Boolean;
}

type PlayerInfo = {
  characters: string[]; // character ID
  name: string;
}

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css'],
})
export class CharactersComponent implements OnInit {
  hasError = false;
  readonly playerCount = 2;
  readonly characterCount = 3;

  characters: CharacterChoice[] = [];
  players: PlayerInfo[] = [];

  currentName: string = "";

  constructor(
    private router: Router,
    private characterService: CharacterService,
  ) {}

  range(i: number): number[] {
    return Array(i).fill(0).map((_, i) => i);
  }

  getCharacters(): void {
    this.characterService.getCharacters().subscribe({
      next: characters => characters.forEach(char => {
        this.characters.push({
          char: char,
          selected: false,
        });
      }),
      error: err => {
        console.error("could not character list: %o", err);
        this.error(err);
      },
    });
  }

  currentPlayer(): number {
    return this.players.length + 1;
  }

  border(char: CharacterChoice): string {
    return char.selected ? "card-selected" : "";
  }

  ngOnInit(): void {
    this.getCharacters();
    console.log(this.characters);
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  onSelect(character: CharacterChoice): void {
    character.selected = !character.selected;
  }

  nextPlayerButton() {
    if (this.currentPlayer() < this.playerCount)
      return "Next";
    else if (this.currentPlayer() === this.playerCount)
      return "Play";
    else
      return "Loading...";
  }

  nextPlayerAction() {
    if (this.currentPlayer() > this.playerCount)
      return; // Waiting for backend response
    const charCount = this.characters.filter(x => x.selected).length;
    if (charCount !== this.characterCount) {
      alert(`You selected ${charCount}/${this.characterCount} characters`);
      return;
    }
    if (!this.currentName) {
      alert("You did not enter any name");
      return;
    }

    this.players.push({
      characters: this.characters.filter(x => x.selected).map(x => x.char.name),
      name: this.currentName,
    });
    this.characters.forEach(x => x.selected = false);

    this.currentName = "";
    if (this.currentPlayer() <= this.playerCount)
      return;

    const characters1 = this.players[0].characters;
    const characters2 = this.players[1].characters;
    const names = this.players.map(x => x.name);
    console.debug("going to arena choice with:", characters1, characters2, names)
    this.router.navigate(['/arena'], {
      queryParams: {
        characters1: characters1,
        characters2: characters2,
        names: names,
      }
    });
  }
}
