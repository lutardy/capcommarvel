import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Session } from '../classes/session';
import { SessionService } from '../services/session.service';
import { Player } from '../classes/player';
import { Character } from '../classes/character';
import { ErrorComponent } from '../error/error.component';
import { Howl } from 'howler';
import { BrowserTestingModule } from '@angular/platform-browser/testing';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css'],
})
export class PlayComponent implements OnInit, OnDestroy {
  hasError = false;
  readonly endRoute = '/end';

  session: bigint;
  sessionInfo?: Session;
  players?: Player[];
  sessionIsInit = false;
  bgm?: Howl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService
  ) {
    const int = parseInt(this.route.snapshot.paramMap.get('session') || '');
    if (isNaN(int)) throw Error('Bad Request');
    this.session = BigInt(int);
  }

  ngOnInit(): void {
    this.updateSessionInfo();
  }

  playMusic(): void {
    console.debug("starting bgm");
    this.bgm = new Howl({
      src: ['../../assets/bgm.mp3'],
      autoplay: true,
      loop: true,
      volume: 0.1,
    });
    this.bgm.play();
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  gray(char: Character): string {
    return char.currentLp <= 0 ? 'filter: grayscale(100%);' : '';
  }

  player(index: number): Player | undefined {
    if (this.players === undefined) return undefined;
    return this.players[index];
  }

  curChar(player: Player): Character {
    return player.charac[player.currentCharacter];
  }

  passiveChars(player: Player): Character[] {
    return player.charac.filter((_, i) => i !== player.currentCharacter) || [];
  }

  isCurrentPlayer(player: Player): Boolean {
    return this.sessionInfo?.current === player.id;
  }

  healthBar(char: Character): number {
    return (char.currentLp / char.lp) * 100;
  }

  ngOnDestroy() :void {
    console.debug("stopping bgm");
    this.bgm?.stop();
  }

  updateSessionInfo(): void {
    this.sessionService.getSession(this.session).subscribe({
      next: (session) => {
        this.sessionInfo = session;
        console.debug('Updated session:', this.sessionInfo);
        this.players = Object.values(this.sessionInfo.players);

        if (this.sessionService.winner(this.sessionInfo) !== null) {
          console.info('got winner, routing to %s', this.endRoute);
          this.router.navigate([this.endRoute + '/' + this.session]);
        }
        if (
         (!this.sessionIsInit) && this.players?.some((p) => p.charac.some((c) => c.name === 'Joker'))
        ) {
          this.playMusic();
          this.sessionIsInit=true;
        }
      },
      error: (err) => {
        console.error('could not get session information: %o', err);
        this.error(err);
      },
    });
  }

  onHeal(player: Player) {
    if (!this.isCurrentPlayer(player)) return;

    this.sessionService.heal(this.session).subscribe({
      next: (x) => {
        console.debug('heal: success');
        this.updateSessionInfo();
      },
      error: (err) => {
        console.error('could not heal: %o', err);
        this.error(err);
      },
    });
  }

  onAttack(player: Player) {
    if (!this.isCurrentPlayer(player)) return;

    this.sessionService.attack(this.session).subscribe({
      next: (x) => {
        console.debug('attack: success');
        this.updateSessionInfo();
      },
      error: (err) => {
        console.error('could not attack: %o', err);
        this.error(err);
      },
    });
  }

  onSwap(player: Player, char: Character): void {
    const index = player.charac.indexOf(char);
    if (index < 0) return;
    console.info(
      'player %s: switching to character %s',
      player.name,
      char.name
    );
    this.sessionService.swap(this.session, index).subscribe({
      next: (_) => {
        console.debug('swap: success');
        this.updateSessionInfo();
      },
      error: (err) => {
        console.error('could not swap to %o: %o', char, err);
        this.error(err);
      },
    });
  }
}
