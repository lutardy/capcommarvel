import { Injectable } from '@angular/core';
import { Character } from '../classes/character';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {

  private characterURL = 'http://localhost:5106/api/Character';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getCharacters(): Observable<Character[]> {
    return this.http.get<Character[]>(this.characterURL)
  }

  postCharacter(charac: Character): Observable<void> {
    return this.http.post<void>(this.characterURL, charac);
  }

  putCharacter(charac: Character): Observable<void> {
    return this.http.put<void>(this.characterURL + '/' + charac.name, charac);
  }

  deleteCharacter(id: string): Observable<void> {
    return this.http.delete<void>(this.characterURL + '/' + id);
  }
}
