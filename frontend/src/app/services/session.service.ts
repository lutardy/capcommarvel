import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Session } from '../classes/session';
import { Observable, of } from 'rxjs';
import { Character } from '../classes/character';
import { Arena } from '../classes/arena';
import { Player } from '../classes/player';

function basename(path: string): string {
  return path.substring(path.lastIndexOf('/') + 1);
}

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  readonly backendBaseUrl = 'http://localhost:5106';
  readonly sessionUrl = this.backendBaseUrl + '/api/session';

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

  winner(session: Session): Player | null {
    const winner = (session?.winner || null) === -1
      ? null : session?.winner;
    if (winner === null)
      return null;

    return Object.values(session.players).find(x => x.id === winner) || null;
  }

  getSession(id: bigint): Observable<Session> {
    const url = this.sessionUrl + '/' + id;
    console.debug("request: GET", url);
    return this.http.get<Session>(url);
  }

  postSession(characters1: string[], characters2: string[], names: string[], arena: number): Observable<bigint> {
    const body = {
      names: names,
      characters1: characters1,
      characters2: characters2,
      arena: arena,
    };
    const url = this.sessionUrl;

    console.debug("request: POST %s: %o", url, body);
    return new Observable(observer => {
      this.http.post(url, body, {observe: 'response',}).subscribe({
        next: resp => {
          const session = resp.headers.get('Id');
          if (session === null) {
            observer.error(`no session ID returned from server`);
            return;
          }
          try {
            observer.next(BigInt(session));
          } catch (error) {
            observer.error(`invalid session ID: ${session}: ${error}`)
            return;
          }
          observer.complete();
        },
        error: error => {
          observer.error(error);
        },
      });
    });
  }

  delete(id: bigint): Observable<void> {
    const url = this.sessionUrl + '/' + id;
    console.debug("request: DELETE %s", url);
    return this.http.delete<void>(url);
  }

  attack(id: bigint): Observable<void> {
    const url = this.sessionUrl + '/' + 'attack' + '/' + id;
    console.debug("request: PUT %s", url);
    return this.http.put<void>(url, null);
  }

  heal(id: bigint): Observable<void> {
    const url = this.sessionUrl + '/' + 'heal' + '/' + id;
    console.debug("request: PUT %s", url);
    return this.http.put<void>(url, null);
  }

  reset(id: bigint): Observable<void> {
    const url = this.sessionUrl + '/' + 'resetsession' + '/' + id;
    console.debug("request: PUT %s", url);
    return this.http.put<void>(url, null);
  }

  swap(id: bigint, index: number): Observable<void> {
    const url = this.sessionUrl + '/' + 'swap' + '/' + id;
    const params = new HttpParams().appendAll({
      newcharac: index,
    });
    console.debug("request: PUT %s\nwith params: %o", url, params);
    return this.http.put<void>(url, null, {params: params});
  }
}
