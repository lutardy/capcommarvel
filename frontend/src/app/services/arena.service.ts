import { Injectable } from '@angular/core';
import { Arena } from '../classes/arena';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ArenaService {

  private arenaURL = 'http://localhost:5106/api/Arena';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getArenas(): Observable<Arena[]> {
    return this.http.get<Arena[]>(this.arenaURL)
  }

  postArena(arena: Arena): Observable<void> {
    return this.http.post<void>(this.arenaURL, arena);
  }

  putArena(arena: Arena): Observable<void> {
    return this.http.put<void>(this.arenaURL + '/' + arena.id, arena);
  }

  deleteArena(id: number): Observable<void> {
    return this.http.delete<void>(this.arenaURL + '/' + id);
  }
}
