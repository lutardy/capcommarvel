import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Character } from '../classes/character';
import { Player } from '../classes/player';
import { SessionService } from '../services/session.service';
import { ErrorComponent } from '../error/error.component';

@Component({
  selector: 'app-endgame',
  templateUrl: './endgame.component.html',
  styleUrls: ['./endgame.component.css']
})
export class EndgameComponent implements OnInit, OnDestroy {
  hasError = false;
  session: bigint;
  winner: Player | null = null;
  cleanup = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService,
  ) {
    const int = parseInt(this.route.snapshot.paramMap.get('session') || '');
    if (isNaN(int)) throw Error('Bad Request');
    this.session = BigInt(int);
   }

  ngOnInit(): void {
    this.sessionService.getSession(this.session).subscribe({
      next: session => {
        console.debug("got session info: %o", session);
        this.winner = this.sessionService.winner(session);
        if (this.winner === null)
          this.error("the game has not finished");
        console.info("winner is: %o", this.winner);
      },
      error: err => {
        console.error("could not get session information: %o", err);
        this.error(err);
      },
    });
  }

  async ngOnDestroy(): Promise<void> {
    if (this.cleanup)
      await lastValueFrom(this.sessionService.delete(this.session));
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  curChar(player?: Player | null): Character | undefined {
    return player?.charac[player.currentCharacter];
  }

  onResetHard(): void {
    this.router.navigate(['/']);
  }

  onResetSoft(): void {
    this.cleanup = false;
    this.sessionService.reset(this.session).subscribe({
      next: x => {
        this.router.navigate(['/play/' + this.session]);
      },
      error: err => {
        console.error("could not reset game: %o", err);
        this.error(err);
      },
    });
  }
}
