import { Character } from './character';

export const CHARACTERS: Character[] = [
  {
    name: 'Ryu',
    lp: 50,
    attack: 50,
    sprite:
      'https://cdn.discordapp.com/attachments/452492621013843968/936627793998004296/latest.png',
    icon: 'https://cdn.discordapp.com/attachments/452492621013843968/936627853854916628/450_1000.png',
  },
  {
    name: 'Captain America',
    lp: 50,
    attack: 50,
    sprite:
      'https://cdn.discordapp.com/attachments/452492621013843968/936626922962714634/latest.png',
    icon: 'https://cdn.discordapp.com/attachments/452492621013843968/936627089036165190/wHqByEgDpwjlQAAAABJRU5ErkJggg.png',
  },
];
