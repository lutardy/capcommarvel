export type Arena = {
  id : number;
  name: string;
  sprite: string;
  icon: string;
  description: string;
}
