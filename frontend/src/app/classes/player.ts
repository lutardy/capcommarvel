import { Character } from "./character";

export type Player = {
    id: number;
    name: string;
    charac: Character[];
    currentCharacter: number;
};
