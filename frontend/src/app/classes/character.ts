export type Character = {
    name: string;
    lp: number;
    attack: number;
    currentLp: number;
    currentAttack: number;
    sprite: string;
    icon: string;
    description: string;
    bonusAttack: number;
    bonusLp: number;
    bonusArena: number;
    hasHealed: Boolean;
  }
