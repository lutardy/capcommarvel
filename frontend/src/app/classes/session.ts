import { Player } from "./player";
import { Arena } from "./arena";

export type Session = {
    id: number;
    players: { [id: number]: Player };
    current: number;
    winner: number;
    arena: Arena;
};
