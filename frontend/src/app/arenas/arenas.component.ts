import { Component, OnInit } from '@angular/core';
import { Arena } from '../classes/arena';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ArenaService } from '../services/arena.service';
import { SessionService } from '../services/session.service';
import { ErrorComponent } from '../error/error.component';

@Component({
  selector: 'app-arenas',
  templateUrl: './arenas.component.html',
  styleUrls: ['./arenas.component.css'],
})
export class ArenasComponent implements OnInit {
  hasError = false;
  arenas: Arena[] = [];

  /* Data from params */
  characters1: string[];
  characters2: string[];
  names: string[];
  playerCount: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private arenaService: ArenaService,
    private sessionService: SessionService,
  ) {
    this.characters1 = this.route.snapshot.queryParamMap.getAll('characters1');
    this.characters2 = this.route.snapshot.queryParamMap.getAll('characters2');
    this.names = this.route.snapshot.queryParamMap.getAll('names');
    this.playerCount = this.names.length;
  }

  getArenas(): void {
    this.arenaService.getArenas().subscribe({
      next: arenas => this.arenas = arenas,
      error: err => {
        console.error("could net get arenas: %o", err);
        this.error(err);
      },
    });
  }

  ngOnInit(): void {
    this.getArenas();
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  onSelect(arena: Arena): void {
    console.debug(
      "Creating session with: player 1: %o, player 2: %o, names: %o, arena %o",
      this.characters1, this.characters2, this.names, arena
    );
    this.sessionService.postSession(this.characters1, this.characters2, this.names, arena.id).subscribe({
      next: session => {
        console.info("created session: ", session);
        this.router.navigate(['/play', session]);
      },
      error: err => {
        console.error("could not create session: %o", err);
        this.error(err);
      },
    });
  }
}
