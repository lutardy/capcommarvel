import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CharactersComponent } from './characters/characters.component';
import { PlayComponent } from './play/play.component';
import { ArenasComponent } from './arenas/arenas.component';
import { EndgameComponent } from './endgame/endgame.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  {path: '', redirectTo: '/new', pathMatch: 'full'},
  {path: 'arena', component: ArenasComponent},
  {path: 'new', component: CharactersComponent},
  {path: 'play/:session', component: PlayComponent},
  {path: 'end/:session', component: EndgameComponent},
  {path: 'admin', component: AdminComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
