import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ErrorComponent } from '../error/error.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  readonly defaultTab = 0;
  hasError = false;
  currentTab: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
  ) {
    let tab = parseInt(this.route.snapshot.queryParamMap.get('default') || '');
    if (isNaN(tab)) {
      tab = this.defaultTab;
      this.updateTabUrl(tab, false);
    }
    this.currentTab = tab;
  }

  ngOnInit(): void {
    this.location.onUrlChange((url, state) => {
      const ourl = new URL(url, window.location.origin);
      const id = Number(ourl.searchParams.get('default')) || this.defaultTab;

      console.debug("url changed to %s: switching to tab %s", url, id);
      this.currentTab = id;
    });
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  updateTabUrl(id: number, updateHistory = true): void {
    const url = '/' + this.route.snapshot.url.map(x => x.path).join('/');
    const query: { [id: string]: string } = {};
    Object.entries(this.route.snapshot.queryParams).forEach(e => query[e[0]] = e[1]);
    query['default'] = `${id}`;
    this.router.navigate([url], { queryParams: query, replaceUrl: !updateHistory });
  }

  onTab(event: MatTabChangeEvent): void {
    const id = event.index;
    console.info("switched to tab %s", id);
    this.updateTabUrl(id);
  }
}
