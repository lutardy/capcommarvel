import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Character } from '../../classes/character';
import { ErrorComponent } from '../../error/error.component';
import { CharacterService } from '../../services/character.service';

function characterForm(char?: Character): any {
  return {
    name: [char?.name],
    lp: [char?.lp],
    attack: [char?.attack],
    sprite: [char?.sprite],
    icon: [char?.icon],
    description: [char?.description],
    bonusAttack: [char?.bonusAttack],
    bonusLp: [char?.bonusLp],
    bonusArena: [char?.bonusArena],
  };
}

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {
  hasError = false;
  characters: Character[] = [];
  newCharacter?: Character;

  characterForms: { [id: string]: FormGroup } = {};
  newCharacterForm = this.formBuilder.group(characterForm());

  constructor(
    private formBuilder: FormBuilder,
    private characterService: CharacterService,
  ) { }

  getCharacters(): void {
    this.characterService.getCharacters().subscribe({
      next: characters => {
        this.characters = characters
        this.characters.forEach(c => {
          this.characterForms[c.name] = this.formBuilder.group(characterForm(c))
        });
      },
      error: err => {
        console.error("could not get character list: %o", err);
        this.error(err);
      },
    })
  }

  ngOnInit(): void {
    this.getCharacters();
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  onCreate() {
    if (!this.newCharacterForm.valid)
      return;
    const charac = this.newCharacterForm.value;

    console.debug("creating character: %o", charac);
    this.characterService.postCharacter(charac).subscribe({
      next: x => {
        console.log("created character: %s", charac.name);
        window.location.reload();
      },
      error: err => {
        console.error("could not create character %s: %o", charac.name, err);
        this.error(err);
        window.alert("Creation failed");
      },
    });
  }

  onUpdate(characOrig: Character) {
    const form = this.characterForms[characOrig.name];
    if (!form.valid)
      return;
    const charac = form.value;

    console.debug("updating character: %o with %o", characOrig, charac);
    this.characterService.putCharacter(charac).subscribe({
      next: x => {
        console.log("updated character: %s", charac.name);
        window.location.reload();
      },
      error: err => {
        console.log("could not update character %s: %o", charac.name, err);
        this.error(err);
        window.alert("Update failed");
      },
    });
  }

  onDelete(charac: Character) {
    if (!window.confirm(`Delete ${charac.name}?`))
      return;

    console.debug("deleting character: %o", charac);
    this.characterService.deleteCharacter(charac.name).subscribe({
      next: x => {
        console.log("deleted character: %s", charac.name);
        window.location.reload();
      },
      error: err => {
        console.error("could not delete character %s: %o", charac.name, err);
        this.error(err);
        window.alert("Deletion failed");
      },
    });
  }
}
