import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Arena } from 'src/app/classes/arena';
import { ArenaService } from 'src/app/services/arena.service';
import { ErrorComponent } from '../../error/error.component';

function arenaForm(arena?: Arena): any {
  return {
    id: [arena?.id],
    name: [arena?.name],
    sprite: [arena?.sprite],
    icon: [arena?.icon],
    description: [arena?.description],
  };
}

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.css']
})
export class ArenaComponent implements OnInit {
  hasError = false;
  arenas: Arena[] = [];

  arenaForms: { [id: number]: FormGroup } = {};
  newArenaForm = this.formBuilder.group(arenaForm());

  constructor(
    private arenaService: ArenaService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.getArenas();
  }

  error(err: any): void {
    ErrorComponent.error = err;
    this.hasError = true;
  }

  getArenas(): void {
    this.arenaService.getArenas().subscribe({
      next: arenas => {
        this.arenas = arenas;
        this.arenas.forEach(a => {
          this.arenaForms[a.id] = this.formBuilder.group(arenaForm(a));
        });
      },
      error: err => {
        console.error("could not get arena list: %o", err);
        this.error(err);
      },
    });
  }

  onCreate() {
    if (!this.newArenaForm.valid)
      return;
    const arena = this.newArenaForm.value;

    console.debug("creating arena: %o", arena);
    this.arenaService.postArena(arena).subscribe({
      next: x => {
        console.log("created arena: %s", arena.id);
        window.location.reload();
      },
      error: err => {
        console.error("could not create arena %s: %o", arena.id, err);
        this.error(err);
        window.alert("Creation failed");
      },
    });
  }

  onUpdate(arenaOrig: Arena) {
    const form = this.arenaForms[arenaOrig.id];
    if (!form.valid)
      return;
    const arena = form.value;

    console.debug("updating arena: %o with %o", arenaOrig, arena);
    this.arenaService.putArena(arena).subscribe({
      next: x => {
        console.log("updated arena: %s", arena.id);
        window.location.reload();
      },
      error: err => {
        console.error("could not update arena %s: %o", arena.id, err);
        this.error(err);
        window.alert("Update failed");
      },
    });
  }

  onDelete(arena: Arena) {
    if (!window.confirm(`Delete ${arena.name} (ID ${arena.id})?`))
      return;

    console.debug("deleting arena: %o", arena);
    this.arenaService.deleteArena(arena.id).subscribe({
      next: x => {
        console.log("deleted arena: %s", arena.id);
        window.location.reload();
      },
      error: err => {
        console.error("could not delete arena %s: %o", arena.id, err);
        this.error(err);
        window.alert("Deletion failed");
      },
    });
  }
}
