import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  static error: any;

  ngOnInit(): void {
  }

  getError(): string {
    return JSON.stringify(ErrorComponent.error, null, 2);
  }
}
