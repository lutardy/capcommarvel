# CapcomMarvel

## Consignes

### Backend

Depuis la racine il faut aller dans `backend/WebApiCapcomMarvel`.

Ici il faut lancer dotnet : `$ dotnet run`.

La version de dotnet utilisé de notre côté était la 6.0.xxx (6.0.102 et 6.0.200).

### Frontend

Depuis la racine, il faut aller dans `frontend`.
Il faut ensuite lancer le serveur angular.

Tout d'abord `$ npm install`.

Puis `$ npm run start`.

La version de node utilisé était 16.x.x (16.14.0 et 16.13.1).
La version de npm était 8.x.x (8.3.1 et 8.1.2).

### Visualiser la solution

Une fois la backend et la frontend lancée, le jeu est visualisable à l'adresse `localhost:4200`.

